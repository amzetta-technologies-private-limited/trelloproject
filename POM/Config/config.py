class TestData:
    BASE_URL = 'https://trello.com/en/login'

    USERNAME = 'sabi65052@gmail.com'
    PASSWORD = 'password@123'
    TITLE = 'Boards | Trello'

    BOARD_TITLE = 'Project'
    HOMEPAGE_TITLE = f'{BOARD_TITLE} | Trello'

    existing_list = []
    all_list = []
    LISTS = ['Not Started', 'In Progress', 'QA', 'Done']
    CARDS = ['Card 1', 'Card 2', 'Card 3', 'Card 4']
    LIST_NAME = 'Not Started'

    # move cards to lists
    CARD_TRANSFER = ['Card 2', 'Card 3', 'Card 2']
    FROM_LIST = ['Not Started', 'Not Started', 'In Progress']
    TO_LIST = ['In Progress', 'QA', 'QA']

    # assign user
    ASSIGN_USER_LIST_FROM = 'Not Started'
    ASSIGN_USER_TRANSFER_CARD = 'Card 1'
    COMMENT = 'I am done'
