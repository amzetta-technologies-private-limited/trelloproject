from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class basePage:

    def __init__(self, driver):
        self.driver = driver

    def click_element(self, locator):
        WebDriverWait(self.driver, 10).until(
            ec.visibility_of_element_located(locator)).click()

    def do_send_keys(self, locator, value):
        WebDriverWait(self.driver, 10).until(
            ec.visibility_of_element_located(locator)).send_keys(value)

    def get_element(self, locator):
        WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located(locator))

    def get_element_text(self, locator):
        element = WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located(locator))
        return element.text

    def get_elements_list(self, locator):
        element = WebDriverWait(self.driver, 10).until(
            ec.presence_of_all_elements_located(locator))
        return element

    def get_pageTitle(self, title):
        WebDriverWait(self.driver, 10).until(
            ec.title_contains(title))
        return self.driver.title

    def is_visible(self, locator):
        element = WebDriverWait(self.driver, 10).until(
            ec.visibility_of_element_located(locator))
        return bool(element)

    def get_screenshot(self, locator, filename):
        return self.driver.save_screenshot(filename)
