from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

from Pages.basePage import basePage
from Config.config import TestData


class homePage(basePage):

    # locators
    CREATE_BOARD = (
        By.XPATH, '//*[@class="board-tile mod-add"]')
    BOARD_TITLE = (
        By.XPATH, '/html/body/div[3]/div/section/div/form/div[1]/label/input')
    CREATE_BTN = (By.XPATH, '/html/body/div[3]/div/section/div/form/button')
    ADD_ANOTHER_LIST = (
        By.XPATH, '//*[@class="open-add-list js-open-add-list"]')
    ADD_LIST = (By.XPATH, (f'//input[@class="list-name-input"]'))
    EXISTING_LIST = (
        By.XPATH, '//*[@class="list-header js-list-header u-clearfix is-menu-shown ui-sortable-handle"]')
    CARD_INPUT = (
        By.XPATH, '//*[@class="list-card-composer-textarea js-card-title"]')
    ADD_CARD_BTN = (
        By.XPATH, '//*[@class="nch-button nch-button--primary confirm mod-compact js-add-card"]')
    FIRST_MOVE_BTN = (
        By.XPATH, '//*[@id="chrome-container"]/div[3]/div/div/div/div[5]/div[5]/div/a[1]')
    FINAL_MOVE_BTN = (
        By.XPATH, '//*[@id="chrome-container"]/div[4]/div/div[2]/div/div/div/div/input')
    LIST_DROPDOWN = (
        By.XPATH, '//select[@class="js-select-list"]/option')
    CURRENT_USER_ICON = (
        By.XPATH, '//*[@id="header"]/div[3]/div[5]/button/div/span')
    CURRENT_USERNAME = (
        By.CLASS_NAME, 'C6mIzhIHAXFKf-')
    MEMBERS_MENU = (
        By.XPATH, '//*[@id="chrome-container"]/div[3]/div/div/div/div[5]/div[2]/div/a[1]/span[2]')
    SEARCH = (
        By.XPATH, '//*[@id="chrome-container"]/div[4]/div/div[2]/div/div/div/input')
    SEARCHED_NAME = (
        By.XPATH, '//*[@class="full-name"]')
    COMMENT_AREA = (
        By.XPATH, '//*[@id="chrome-container"]/div[3]/div/div/div/div[4]/div[11]/div[2]/form/div/div/textarea')
    SAVE_BTN = (
        By.XPATH, '//*[@id="chrome-container"]/div[3]/div/div/div/div[4]/div[11]/div[2]/form/div/div/div[1]/input')
    CLOSE_BTN = (By.XPATH, '//*[@id="chrome-container"]/div[3]/div/div/a')

    # constructor of page class

    def __init__(self, driver):
        super().__init__(driver)

    def create_board(self, boardtitle):
        self.click_element(self.CREATE_BOARD)
        self.do_send_keys(self.BOARD_TITLE, boardtitle)
        self.click_element(self.CREATE_BTN)

    def add_list(self, arr):
        self.click_element(self.ADD_ANOTHER_LIST)
        for m in arr:
            self.click_element(self.ADD_LIST)
            self.do_send_keys(self.ADD_LIST, m)
            self.do_send_keys(self.ADD_LIST, Keys.ENTER)
        e_list = self.get_elements_list(self.EXISTING_LIST)
        for y in e_list:
            TestData.all_list.append(y.text)
        for d in arr:
            TestData.all_list.append(d)

    def add_cards(self, list_name):
        lst_count = TestData.all_list.index(list_name) + 1
        ADD_THE_CARD = By.XPATH, (
            f'//*[@id="board"]/div[{lst_count}]/div/div[3]/a/span[2]')
        self.click_element(ADD_THE_CARD)
        for card_name in TestData.CARDS:
            self.do_send_keys(self.CARD_INPUT, card_name)
            self.click_element(self.ADD_CARD_BTN)

    def move_cards(self, card_name, from_list, to_list):  # move the crads to respected list
        lst_count = TestData.all_list.index(from_list) + 1
        for i in range(len(TestData.CARDS)):
            cardss = i + 1
            cards_ = By.XPATH, (
                f'//*[@id="board"]/div[{lst_count}]/div/div[2]/a[{cardss}]/div[3]/span')
            time.sleep(1)
            c = self.get_element_text(cards_)
            if (card_name == c):
                self.click_element(cards_)
                break
        self.click_element(self.FIRST_MOVE_BTN)
        time.sleep(1)
        list_dropdown = self.driver.find_elements(
            By. XPATH, '//select[@class="js-select-list"]/option')
        for i in list_dropdown:
            listname = i.text
            if (to_list == listname):
                i.click()
                break
        self.click_element(self.FINAL_MOVE_BTN)
        self.click_element(self.CLOSE_BTN)

    def assign_user(self, from_list, card_name, comment):
        self.click_element(self.CURRENT_USER_ICON)
        time.sleep(1)
        user = self.get_element_text(self.CURRENT_USERNAME)
        lst_count = TestData.all_list.index(from_list) + 1
        for i in range(len(TestData.CARDS)):
            cardss = i + 1
            cards_ = By.XPATH, (
                f'//*[@id="board"]/div[{lst_count}]/div/div[2]/a[{cardss}]/div[3]/span')
            time.sleep(1)
            c = self.get_element_text(cards_)
            if (card_name == c):
                self.click_element(cards_)
                break
        self.click_element(self.MEMBERS_MENU)
        self.do_send_keys(self.SEARCH, user)
        self.click_element(self.SEARCHED_NAME)
        self.do_send_keys(self.COMMENT_AREA, comment)
        self.click_element(self.SAVE_BTN)
        self.click_element(self.CLOSE_BTN)
