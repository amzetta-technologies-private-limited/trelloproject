from selenium.webdriver.common.by import By

from Pages.basePage import basePage
from Pages.homePage import homePage
from Config.config import TestData


class loginPage(basePage):

    # locators
    USERNAME = (By.XPATH, "//*[@id='user']")
    PASSWORD = (By.XPATH, '//*[@id="password"]')
    LOGIN_CONTINUE_BTN = (By.XPATH, "//*[@id='login']")
    LOGIN_BTN = (By.XPATH, "//*[@id='login-submit']")
    ANCHOR_ELEMENT = (By.XPATH, '//*[@id="header"]/a/div/div')

    # constructor of page class
    def __init__(self, driver):
        super().__init__(driver)
        self.driver.get(TestData.BASE_URL)
        driver.maximize_window()

    def login_webpage(self, username, password):
        self.do_send_keys(self.USERNAME, username)
        self.click_element(self.LOGIN_CONTINUE_BTN)
        self.do_send_keys(self.PASSWORD, password)
        self.click_element(self.LOGIN_BTN)
        return homePage(self.driver)
