from Tests.test_basePage import BaseTest
from Pages.loginPage import loginPage
from Pages.homePage import homePage
from Config.config import TestData


class Test_homePage(BaseTest):

    def test_createBoard(self):
        self.loginPage = loginPage(self.driver)
        home_page = self.loginPage.login_webpage(
            TestData.USERNAME, TestData.PASSWORD)
        home_page.create_board(TestData.BOARD_TITLE)
        title = self.loginPage.get_pageTitle(TestData.HOMEPAGE_TITLE)
        assert title == TestData.HOMEPAGE_TITLE

    def test_addList(self):
        self.homePage = homePage(self.driver)
        self.homePage.add_list(TestData.LISTS)

    def test_addCards(self):
        self.homePage = homePage(self.driver)
        self.homePage.add_cards(TestData.LIST_NAME)

    def test_moveCards(self):
        self.homePage = homePage(self.driver)
        for i in range(0, len(TestData.CARD_TRANSFER)):
            self.homePage.move_cards(
                TestData.CARD_TRANSFER[i], TestData.FROM_LIST[i], TestData.TO_LIST[i])

    def test_assignUser(self):
        self.homePage = homePage(self.driver)
        self.homePage.assign_user(
            TestData.ASSIGN_USER_LIST_FROM, TestData.ASSIGN_USER_TRANSFER_CARD, TestData.COMMENT)
