from Tests.test_basePage import BaseTest
from Pages.loginPage import loginPage
from Config.config import TestData


class Test_loginPage(BaseTest):

    # Test cases
    def test_loginPage(self):
        self.loginPage = loginPage(self.driver)
        self.loginPage.login_webpage(TestData.USERNAME, TestData.PASSWORD)
        title = self.loginPage.get_pageTitle(TestData.TITLE)
        assert title == TestData.TITLE
