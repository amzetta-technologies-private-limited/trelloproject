import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(params=['chrome'], scope='class')
def init_driver(request):
    driver = webdriver.Chrome(ChromeDriverManager().install())
    request.cls.driver = driver
    driver.implicitly_wait(10)
    yield
    driver.close()
